%% Function for the means after participantsetup function

function OverallMeans(file, expConds)

otherMan = expConds.OtherName; %variable name of the other manipulation condition (RSVP type or visual distinctiveness)

load(file);

row = 0; %doing this to keep track of subject row bc of mean fieldnames
for s = 1:numel(fieldnames(data)) %data is in file from participant setup
    
    subjNames = fieldnames(data);
    subj = string(subjNames(s));
    
    if contains(subj, 'p' ) == 0      
        %do nothing bc I'm going to put means in here and that's not a person          
    else
        
        disp(['Means: ID #' num2str(s) ' / ' num2str(numel(subjNames)-2) ': ' subj]);
        
        %only give it a row if it's a person
        row = row + 1;
        
        data.Means(row).ID = data.(subj).info.ID;
        
        %getting lag, vals
        lags = unique(([data.(subj).trials.Lag])); 
        vals = unique(([data.(subj).trials.Val])); 
        others = unique(([data.(subj).trials.(otherMan)])); 
        
        %for name purposes later
        valLetters = expConds.Vals;
        otherLetters = expConds.Other;
        
        for l = 1:size(lags,2)
            for v = 1:size(vals,2)
                for o = 1:size(others, 2)                    

                    %getting current conditions
                    lag = lags(l);
                    val = vals(v);
                    oth = others(o);
                    valLetter = valLetters(v);
                    otherLetter = otherLetters(o);
                    
                    %name for structure head
                    condName = string([valLetter '_' otherLetter '_L' num2str(lag)]);

                    %participants' means
                    data.Means(row).(condName) = nanmean([data.(subj).trials([data.(subj).trials.Lag] == lag & [data.(subj).trials.Val] == val & [data.(subj).trials.(otherMan)] == oth).Accuracy]);
                        %disp(condName)
                        %disp(size([data.(subj).trials([data.(subj).trials.Lag] == lag & [data.(subj).trials.Val] == val & [data.(subj).trials.VisDist] == vis).Accuracy]))
                end
            end
        end
    end
    save(file, 'data', 'no', 'expConds');
end

%% Overall mean (average everyone)
for l = 1:size(lags,2)
    for v = 1:size(vals,2)
        for o = 1:size(others, 2)                    

            %getting current conditions
            lag = lags(l);
            valLetter = valLetters(v);
            otherLetter = otherLetters(o);

            %name for structure head
            condName = string([valLetter '_' otherLetter '_L' num2str(lag)]);

            %mean
            data.Overall.Mean.(condName) = mean([data.Means(:).(condName)]);
        end
    end
end
save(file, 'data', 'no', 'expConds');

end


















