%% Flattening the baseline condition slope and adjusting the other conditions to match (to make better blinks for AB_fitting)

function AB_BaselineDetrend(file, expConds)

otherMan = expConds.OtherName; %variable name of the other manipulation condition (RSVP type or visual distinctiveness)
baseline = expConds.Baseline;

load(file);

xData = expConds.Lags;
vals = expConds.Vals;
others = expConds.Other;

%% per participant
for p = 1:numel(data.Means)

    ID = data.Means(p).ID;
    data.Detrend.Means(p).ID = ID;
    
    disp(['ABdetrend: ID #', num2str(p), ' / ', num2str(numel(data.Means)), ': ', num2str(ID)]);
    
    %using the baseline condition to find slope stuff
    for lag = 1:size(xData,2)
        Bcolname = [baseline '_L' num2str(xData(lag))];
        Bdata(lag) = data.Means(p).(Bcolname);
    end
    B_coefficients = polyfit(xData, Bdata, 1);
    detLine = polyval([B_coefficients(1) 0], xData);
            
    for val = 1:size(vals,2)            
        for oth = 1:size(others,2)
            yData = [];      
            %oldYdata = [];
            for lag = 1:size(xData,2)
                colname = [vals(val) '_' others(oth) '_L' num2str(xData(lag))];
                oldYdata(size(yData,2)+1) = data.Means(p).(colname);
                currPoint = data.Means(p).(colname) - detLine(lag); %detrended
                yData(size(yData,2)+1) = currPoint;
                data.Detrend.Means(p).(colname) = currPoint;
            end      
            
            colName = [vals(val) '_' others(oth)];
            
%             figure();
%             hold on
%             plot(xData, oldYdata, '-r');
%             yData_Interim = yData;
            yData = yData - max(yData) + 1;
%             plot(xData, yData_Interim, '--b');
%             plot(xData, yData, '--g');
%             title([colName, num2str(ID)]);

            [paramEst R2 predictedData]=paramABfit(xData,yData);
            widthExp = exp(paramEst(2)); %backwards log to get size in lags
%             plotName = [colName, num2str(ID)];
%             paramABplot(paramEst,xData,yData,plotName);

            data.Detrend.ABfit.Sparing(p).([colName '_Spar']) = paramEst(1);
            data.Detrend.ABfit.Width(p).([colName '_W']) = paramEst(2);
            data.Detrend.ABfit.WidthExp(p).([colName '_Wexp']) = widthExp;
            data.Detrend.ABfit.Min(p).([colName '_M']) = paramEst(3);
            data.Detrend.ABfit.Amp(p).([colName '_A']) = paramEst(4);
            data.Detrend.ABfit.R2(p).([colName '_R2']) = R2;
            data.Detrend.ABfit.PD(p).([colName '_PD']) = predictedData;

            clearvars paramEst R2 predictedData
        end
    end    

save(file, 'data', 'no', 'expConds');
end