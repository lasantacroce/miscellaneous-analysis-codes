%% Setting up function for participant setup

function ParticipantSetup_words(file, filefolder, numTrials, expConds)

otherMan = expConds.OtherName; %variable name of the other manipulation condition (RSVP type or visual distinctiveness)

filelist = dir(filefolder);

for f = numel(filelist):-1:1
    if contains(filelist(f).name, '.csv') == 0
        filelist(f) = [];        
    elseif contains(filelist(f).name, 'PARTICIPANT') == 1
        filelist(f) = [];        
    end    
end   

%getting rid of duplicates
for i = 1:numel(filelist)
    currID = filelist(i).name;
    IDs(i, 1) = str2num(currID(1:5));
    IDs(i, 2) = filelist(i).bytes;
end
for i = size(IDs, 1):-1:1
    x = [];
    x = find(IDs(:,1) == IDs(i,1));
    if size(x, 1) > 1
        if IDs(i, 2) > IDs(x(1,1), 2)
            IDs(x(1,1), :) = [];
            filelist(x(1,1)) = [];
        else
            IDs(i, :) = [];
            filelist(i) = [];
        end
    end
end

%try to load previous saved file, create one if it doesn't exist
try
    load(file, 'data', 'no', 'expConds');
catch
    data = struct();
    no.ID = 1;
    no.Why = "no";
    save(file, 'data', 'no', 'expConds');
end

load(file); 
try 
    load('targetSpellings.mat');
catch
end

%% Reading in

for s = 1:numel(filelist)
    
    %ID
    IDchar = strcat('p', filelist(s).name(1:5));
    ID = str2num(filelist(s).name(1:5));
    
    %only run if it isn't a bad participant (few trials, poor performance, etc.)
    if ismember(ID, [no.ID]) == 1 || ismember(IDchar, fieldnames(data)) == 1
        
        %displaying in console bc loading excels take f o r e v e r and I want to know where I am
        disp(['Setup: ID #', num2str(s), ' / ', num2str(numel(filelist)), ': ', num2str(ID), '  -  skipped']);
    else
        %displaying in console bc loading excels take f o r e v e r and I want to know where I am
        disp(['Setup: ID #', num2str(s), ' / ', num2str(numel(filelist)), ': ', num2str(ID)]);
    
        %reading in csv
        fullCSV = readtable(filelist(s).name);
        
        %time in experiment
        try
            startTime = fullCSV.x___startTime(1);
            endTimeAll = fullCSV.Time(find(~cellfun(@isempty, fullCSV.Time(:))));
            endTime = str2num(string(endTimeAll(end)));
            if endTime < startTime %if they went over midnight -_-
                endTime = endTime + 1440; %1440 is the number of minutes in a day
            end
            expTime = endTime - startTime;
        catch
            expTime = 0;
        end
        
        %number of trials
        if ismember('Trial_thisRepN', fullCSV.Properties.VariableNames) %bc this doesn't exist in the training
            nTrials = sum(~isnan(fullCSV.Trial_thisRepN));
        else
            nTrials = 0;
        end
              
        %only use participant if they finished
        if nTrials < numTrials
            noSize = numel([no.ID]) + 1;
            no(noSize).ID = ID;
            no(noSize).Why = strcat('nTrials (', num2str(nTrials), ')');            
        else
            
            data.(IDchar).info.nTrials = nTrials;
            
            data.(IDchar).info.expTime = expTime;

            %finding the training trials to delete them + make a new table
            trainingEnd = find(isnan(fullCSV.keyExpStart_rt) == 0); %because this is the button they press to begin the first real trial and the RT is a number
            newCSV = fullCSV(trainingEnd:end,:);     
                    
            %doing this in case they left answers blank (this shouldn't happen)
            for i = 1:size(newCSV, 1)
                if size(newCSV.keySpace_keys{i}, 1) > 0
                    if size(newCSV.Response{i},1) == 0
                        newCSV.Response{i} = 'x';
                    end
                end
            end  

            %% making structure for data

            data.(IDchar).info.ID = ID;
            
            %computer info 
            data.(IDchar).info.frameRate = fullCSV.frameRate(1);
            data.(IDchar).info.stimFrames = fullCSV.StimFrames(1);
            data.(IDchar).info.browser = fullCSV.Browser(1);
            data.(IDchar).info.OS = fullCSV.OS(1);           
            
            %stimTime
            timeText = char(fullCSV.start_stop(56)); 
            timeText2 = timeText(4:end-1);
            stimTime = str2num(timeText2);
            data.(IDchar).info.stimTime = stimTime;
            
            %% All trial info to call to later with t (consolidating info bc nans)

            %conditions
            CDIlag_order = newCSV.T1lag(~isnan(newCSV.T1lag)); %CDI position
            Lag_order = newCSV.Lag(~isnan(newCSV.Lag)); %CDI --> target lag
            Val_order = newCSV.Valence(~isnan(newCSV.Valence)); %CDI valence
            Other_order = newCSV.(otherMan)(~isnan(newCSV.(otherMan))); %other manipulation (like vis distinct or RSVP type)         
            CDI_order = newCSV.CDI(find(~cellfun(@isempty, newCSV.CDI(:)))); %CDI name

            %RSVP stream
            RSVP1 = newCSV.RSVP_Stream(find(~cellfun(@isempty, newCSV.RSVP_Stream(:))));
            RSVP2 = regexp(RSVP1, ',', 'split');
            RSVPstream_order = vertcat(RSVP2{:});

            %target info
            Targ_order = newCSV.Target(find(~cellfun(@isempty, newCSV.Target(:)))); %targets    
            for ta = 1:size(Targ_order, 1)    
                target = string(Targ_order{ta});
                newTarg = erase(target, "#");
                Target_order(ta,1) = newTarg;
            end
            TheirResponse_order = newCSV.Response(find(~cellfun(@isempty, newCSV.Response(:)))); %their response  
            
            %accuracy
            for acc = 1:size(Target_order, 1)
                
                targChar = char(Target_order(acc)); %to call to correct structure
                
                if any(strcmpi(targetSpellings.(targChar), TheirResponse_order(acc)))
                    Accuracy_order(acc,1) = 1;
                else
                    Accuracy_order(acc,1) = 0;
                end
            end

            %trial info
            for t = 1:nTrials 

                %all the stuff
                data.(IDchar).trials(t).trial = t; %trial #
                data.(IDchar).trials(t).CDIlag = CDIlag_order(t); 
                data.(IDchar).trials(t).Lag = Lag_order(t); 
                data.(IDchar).trials(t).Val = Val_order(t); 
                data.(IDchar).trials(t).(otherMan) = Other_order(t); 
                data.(IDchar).trials(t).CDI = CDI_order(t); 
                data.(IDchar).trials(t).Target = Target_order(t); 
                data.(IDchar).trials(t).TheirResponse = TheirResponse_order(t); 
                data.(IDchar).trials(t).Accuracy = Accuracy_order(t); 
                data.(IDchar).trials(t).RSVPstream = RSVPstream_order(t,:); 
            end
        end        
    end            
    save(file, 'data', 'no', 'expConds'); 
end
end









