%% Participant Setup for Image Experiments

function ParticipantSetup_images(file, filefolder, numTrials, expConds)

otherMan = expConds.OtherName; %variable name of the other manipulation condition (RSVP type or visual (otherMan))

filelist = dir(filefolder);

for f = numel(filelist):-1:1
    if contains(filelist(f).name, '.csv') == 0
        filelist(f) = [];        
    elseif contains(filelist(f).name, 'PARTICIPANT') == 1
        filelist(f) = [];        
    end    
end   

%getting rid of duplicates
for i = 1:numel(filelist)
    currID = filelist(i).name;
    IDs(i, 1) = str2num(currID(1:5));
    IDs(i, 2) = filelist(i).bytes;
end
for i = size(IDs, 1):-1:1
    x = [];
    x = find(IDs(:,1) == IDs(i,1));
    if size(x, 1) > 1
        if IDs(i, 2) > IDs(x(1,1), 2)
            IDs(x(1,1), :) = [];
            filelist(x(1,1)) = [];
        else
            IDs(i, :) = [];
            filelist(i) = [];
        end
    end
end

%try to load previous saved file, create one if it doesn't exist
try
    load(file, 'data', 'no');
catch
    data = struct();
    no.ID = 1;
    no.Why = "no";
    save(file, 'data', 'no');
end



%% Reading in

for s = 1:numel(filelist)
    
    %ID
    IDchar = strcat('p', filelist(s).name(1:5));
    ID = str2double(filelist(s).name(1:5));
    
    %only run if it isn't a bad participant (few trials, poor performance, etc.)
    if ismember(ID, [no.ID]) == 1 || ismember(IDchar, fieldnames(data)) == 1
        
        %displaying in console bc loading excels takes f o r e v e r and I want to know where I am
        disp(['Setup: ID #', num2str(s), ' / ', num2str(numel(filelist)), ': ', num2str(ID), '  -  skipped']);
    else
        %displaying in console bc loading excels takes f o r e v e r and I want to know where I am
        disp(['Setup: ID #', num2str(s), ' / ', num2str(numel(filelist)), ': ', num2str(ID)]);
        
        %reading in csv
        opts=detectImportOptions(filelist(s).name); %bc readtable doesn't work all of a sudden        
        fullCSV = readtable(filelist(s).name, opts);
        
        %time in experiment
        try
            expTimes = fullCSV.TimePassed(~isnan(fullCSV.TimePassed));
            expTime = expTimes(end);
        catch
            expTime = 0;
        end
        
        %number of trials
        if ismember('Trial_thisRepN', fullCSV.Properties.VariableNames) %bc this doesn't exist in the training
            nTrials = sum(~isnan(str2double(fullCSV.Trial_thisRepN)));
        else
            nTrials = 0;
        end
              
        %only use participant if they finished
        if nTrials < numTrials
            %add them to 'no' struct if they didn't
            noSize = numel([no.ID]) + 1;
            no(noSize).ID = ID;
            no(noSize).Why = strcat('nTrials (', num2str(nTrials), ')');            
        else
            
            %finding the training trials to delete them + make a new table
            trainingEnd = find(~isnan(str2double(fullCSV.keyExpStart_rt))); %because this is the button they press to begin the first real trial and the RT is a number
            newCSV = fullCSV(trainingEnd:end,:);          

            %% making structure for data
            data.(IDchar).info.ID = ID;
            data.(IDchar).info.nTrials = nTrials;            
            data.(IDchar).info.expTime = expTime;
            
            %computer info 
            data.(IDchar).info.frameRate = fullCSV.frameRate(1);
            data.(IDchar).info.stimFrames = fullCSV.StimFrames(1);
            data.(IDchar).info.browser = fullCSV.Browser(1);
            data.(IDchar).info.OS = fullCSV.OS(1);
            
            %stimTime
            timeText = char(fullCSV.start_stop(56)); 
            timeText2 = timeText(4:end-1);
            stimTime = str2double(timeText2);
            data.(IDchar).info.stimTime = stimTime;
            
            %all trial info to call to later with t (consolidating info bc nans)
            %conditions
            CDIlag_order = newCSV.T1lag(find(~isnan(str2double(newCSV.T1lag(:))))); %CDI position
            Lag_order = newCSV.Lag(find(~isnan(str2double(newCSV.Lag(:))))); %CDI --> target lag
            Val_order = newCSV.Valence(find(~isnan(str2double(newCSV.Valence(:))))); %CDI valence     
            Other_order = newCSV.(otherMan)(~isnan(str2double(newCSV.(otherMan)))); %CDI valence            
            CDI_order = newCSV.CDI(find(~cellfun(@isempty, newCSV.CDI(:)))); %CDI image name

            %response array setup (because I didn't record the actual image name they clicked, only the position)
            Array1 = newCSV.Array_Setup(find(~cellfun(@isempty, newCSV.Array_Setup(:)))); 
            Array2 = regexp(Array1, ',', 'split');
            ArraySetup_order =  vertcat(Array2{:});

            %RSVP stream
            RSVP1 = newCSV.RSVP_Stream(find(~cellfun(@isempty, newCSV.RSVP_Stream(:))));
            RSVP2 = regexp(RSVP1, ',', 'split');
            RSVPstream_order = vertcat(RSVP2{:});

            %target info
            Target_order = newCSV.Target(find(~cellfun(@isempty, newCSV.Target(:)))); %target image name
            TargetArrayPos_order = newCSV.Target_Position(find(~cellfun(@isempty, newCSV.Target_Position(:)))); %position of target in the response array
%             for tap = 1:size(TargetArrayPos_order, 1)
%                 imname = char(TargetArrayPos_order(tap));
%                 if size(imname, 2) == 11
%                     newimname = imname(3:9);
%                 else
%                     newimname = imname(3:8);
%                 end
%                 TargetArrayPos_order(tap) = {newimname};
%             end                

            %their response
            TheirResponseArrayPos_order = newCSV.mouseResp_clicked_name(find(~cellfun(@isempty, newCSV.mouseResp_clicked_name(:)))); %the image array position that they selected        
            Accuracy_order = newCSV.Response_Correct(find(~isnan(str2double(newCSV.Response_Correct(:)))));

            %trial info
            for t = 1:nTrials 
                %all the stuff
                data.(IDchar).trials(t).trial = t; %trial #
                data.(IDchar).trials(t).CDIlag = str2double(CDIlag_order(t)); 
                data.(IDchar).trials(t).Lag = str2double(Lag_order(t)); 
                data.(IDchar).trials(t).Val = str2double(Val_order(t)); 
                data.(IDchar).trials(t).(otherMan) = str2double(Other_order(t)); 
                data.(IDchar).trials(t).CDI = CDI_order(t); 
                data.(IDchar).trials(t).Target = Target_order(t); 
%                 data.(IDchar).trials(t).TargetArrayPos = TargetArrayPos_order(t); 
%                 data.(IDchar).trials(t).TheirResponse = TheirResponse_order(t); 
                data.(IDchar).trials(t).TheirResponseArrayPos = TheirResponseArrayPos_order(t); 
                data.(IDchar).trials(t).Accuracy = str2double(Accuracy_order(t)); 
                data.(IDchar).trials(t).RSVPstream = RSVPstream_order(t,:); 
                data.(IDchar).trials(t).ResponseArray = ArraySetup_order(t,:); 
            end
        end 
    end            
end
save(file, 'data', 'no');