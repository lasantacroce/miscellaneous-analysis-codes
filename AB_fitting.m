%% AB fitting

function AB_fitting(file, expConds)

otherMan = expConds.OtherName; %variable name of the other manipulation condition (RSVP type or visual distinctiveness)

load(file);

xData = expConds.Lags;
vals = expConds.Vals;
others = expConds.Other;

%% per participant
for p = 1:numel(data.Means)

    ID = data.Means(p).ID;
    
    disp(['ABfit: ID #', num2str(p), ' / ', num2str(numel(data.Means)), ': ', num2str(ID)]);
    
%     data.ABfit.PD(p).ID = ID;
    
    for val = 1:size(vals,2)            
        for oth = 1:size(others,2)
            yData = [];                
            for lag = 1:size(xData,2)
                colname = [vals(val) '_' others(oth) '_L' num2str(xData(lag))];
                yData(size(yData,2)+1) = data.Means(p).(colname);
            end
            
            if numel(unique(yData)) == 1 %bc it freaks out if they're all the same number
                yData(1) = yData(1) + 0.0001;
            end               
            
            colName = [vals(val) '_' others(oth)];

            [paramEst R2 predictedData]=paramABfit(xData,yData);
            widthExp = exp(paramEst(2)); %backwards log to get size in lags
%             plotName = [colName, num2str(ID)];
%             paramABplot(paramEst,xData,yData,plotName);

            data.ABfit.Sparing(p).([colName '_Spar']) = paramEst(1);
            data.ABfit.Width(p).([colName '_W']) = paramEst(2);
            data.ABfit.WidthExp(p).([colName '_Wexp']) = widthExp;
            data.ABfit.Min(p).([colName '_M']) = paramEst(3);
            data.ABfit.Amp(p).([colName '_A']) = paramEst(4);
            data.ABfit.R2(p).([colName '_R2']) = R2;
            data.ABfit.PD(p).([colName '_PD']) = predictedData;

            clearvars paramEst R2 predictedData
        end
    end    

save(file, 'data', 'no', 'expConds');
end

%% Overall
for val = 1:size(vals,2)            
    for oth = 1:size(others,2)
        yData = [];                
        for lag = 1:size(xData,2)
            colname = [vals(val) '_' others(oth) '_L' num2str(xData(lag))];
            yData(size(yData,2)+1) = data.Overall.Mean.(colname);
        end

        if numel(unique(yData)) == 1 %bc it freaks out if they're all the same number
            yData(1) = yData(1) + 0.0001;
        end               

        colName = [vals(val) '_' others(oth)];

        [paramEst R2 predictedData]=paramABfit(xData,yData);
        widthExp = exp(paramEst(2)); %backwards log to get size in lags
        plotName = colName;
        paramABplot(paramEst,xData,yData,plotName);

        data.Overall.ABfit.([colName '_Spar']) = paramEst(1);
        data.Overall.ABfit.([colName '_W']) = paramEst(2);
        data.Overall.ABfit.([colName '_Wexp']) = widthExp;
        data.Overall.ABfit.([colName '_M']) = paramEst(3);
        data.Overall.ABfit.([colName '_A']) = paramEst(4);
        data.Overall.ABfit.([colName '_R2']) = R2;
        data.Overall.ABfit.([colName '_PD']) = predictedData;

        clearvars paramEst R2 predictedData
    end
end    
save(file, 'data', 'no', 'expConds');
end




